﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/*Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
naprednih (sin, cos, log, sqrt...) operacija. */
namespace AnalizaA
{ 
    public partial class Form1 : Form
    {
        
        private string operacija;
        private double mOp1, mOp2, mResult;
        public Form1()
        {
            InitializeComponent();
        }

        private void button0_Click(object sender, EventArgs e)
        {
            
            richTextBox1.AppendText("0");
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            operacija = "+";
            try
            {
                mOp1 = Convert.ToDouble(richTextBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogrešan unos operanda!", "Greška!");
            }
            richTextBox1.Text = "";
        }

        private void buttonSubtract_Click(object sender, EventArgs e)
        {
            operacija = "-";
            try
            {
                mOp1 = Convert.ToDouble(richTextBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogrešan unos operanda!", "Greška!");
            }
            richTextBox1.Text = "";
        }

        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            operacija = "*";
            try
            {
                mOp1 = Convert.ToDouble(richTextBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogrešan unos operanda!", "Greška!");
            }
            richTextBox1.Text = "";
        }

        private void buttonDivide_Click(object sender, EventArgs e)
        {
            operacija = "/";
            try
            {
                mOp1 = Convert.ToDouble(richTextBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogrešan unos operanda!", "Greška!");
            }
            richTextBox1.Text = "";
        }

        private void buttonDot_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText(".");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("2");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("3");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("4");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("5");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("6");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("7");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("8");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("9");
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            mOp1 = 0;
            mOp2 = 0;
            mResult = 0;
            textBoxResult.ResetText();
            richTextBox1.ResetText();
        }

        private void buttonSin_Click(object sender, EventArgs e)
        {
            try
            {
                operacija = "Sin";
                mOp1 = Convert.ToDouble(richTextBox1.Text);
            }
            catch (Exception) 
            {
                MessageBox.Show("Pogrešan unos operanda!", "Greška!");
            }
            richTextBox1.Text = "";
        }

        private void buttonCos_Click(object sender, EventArgs e)
        {
            try
            {
                operacija = "Cos";
                mOp1 = Convert.ToDouble(richTextBox1.Text);
            }
            catch(Exception) 
            {
                MessageBox.Show("Pogrešan unos operanda!", "Greška!");
            }
            richTextBox1.Text = "";
        }

        private void buttonTan_Click(object sender, EventArgs e)
        {
            try
            {
                operacija = "Tan";
                mOp1 = Convert.ToDouble(richTextBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogrešan unos operanda!", "Greška!");
            }
            richTextBox1.Text = "";
        }

        private void buttonPower_Click(object sender, EventArgs e)
        {
            try
            {
                operacija = "Pow";
                mOp1 = Convert.ToDouble(richTextBox1.Text);
            }
            catch(Exception)
            {
                MessageBox.Show("Pogrešan unos operanda!", "Greška");
            }
            richTextBox1.Text = "";
        }

        private void buttonSqRoot_Click(object sender, EventArgs e)
        {
            try
            {
                operacija = "Sqrt";
                mOp1 = Convert.ToDouble(richTextBox1.Text);
            }
            catch(Exception)
            {
                MessageBox.Show("Pogrešan unos operanda!", "Greška");
            }
            richTextBox1.Text = "";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.Text = richTextBox1.Text.Substring(0, richTextBox1.Text.Length - 1);
            }
            catch (Exception) {
                MessageBox.Show("Polje je već prazno!","Greška!");
            }
        }

        private void buttonEquals_Click(object sender, EventArgs e)
        {
            try
            {
                mOp2 = Convert.ToDouble(richTextBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogrešan unos operanda", "Greška!");
            }

            switch (operacija)
            {
                case "+":
                    mResult = mOp1 + mOp2;
                    break;
                case "-":
                    mResult = mOp1 - mOp2;
                    break;
                case "*":
                    mResult = mOp1 * mOp2;
                    break;
                case "/":
                    mResult = mOp1 / mOp2;
                    break;
                case "Sin":
                    mResult = Math.Sin(mOp1);
                    break;
                case "Cos":
                    mResult = Math.Cos(mOp1);
                    break;
                case "Tan":
                    mResult = Math.Tan(mOp1);
                    break;
                case "Pow":
                    mResult = Math.Pow(mOp1, 2);
                    break;
                case "Sqrt":
                    mResult = Math.Sqrt(mOp1);
                    break;
            }

            textBoxResult.Text = mResult.ToString();
            richTextBox1.ResetText();

        }
    }
}
